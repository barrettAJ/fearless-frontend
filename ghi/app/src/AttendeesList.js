import React from 'react'
export default function AttendeesList(props){
  console.log('props',props)
  if (!props.attendees) {
    return null; // or you can return a loading state or an error message
  }
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Conference</th>
        </tr>
      </thead>
      <tbody>
        {props.attendees.map(attendee => (
          <tr key={attendee.href}>
            <td>{attendee.name}</td>
            <td>{attendee.conference}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
