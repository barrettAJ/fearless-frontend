import logo from './logo.svg';
import './App.css';
import React from 'react';
import Nav from './Nav';
import AttendeesList from './AttendeesList.js';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeForm from './AttendeeForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { BrowserRouter, Route, Routes } from "react-router-dom";

function App({attendees}) {
  if (attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
    <Nav />
    {/* <div className="container"> */}
      {/* <LocationForm /> */}
      <Routes>
        <Route index element={<MainPage />} />
        <Route path='locations'>
          <Route path='new' element={<LocationForm/>}/>
        </Route>
        <Route path='conferences'>
          <Route path='new' element={<ConferenceForm/>}/>
        </Route>
        <Route path='attendees'>
          <Route path='' element={<AttendeesList attendees={attendees} />} />
          <Route path='new' element={<AttendeeForm />} />
        </Route>
        <Route path='presentations'>
          <Route path='new' element={<PresentationForm />} />
        </Route>
      {/* <ConferenceForm /> */}
      {/* <AttendeeForm /> */}
      </Routes>
    {/* <AttendeesList attendees={attendees} /> */}
    {/* </div> */}
    </BrowserRouter>
  );
}

export default App;
